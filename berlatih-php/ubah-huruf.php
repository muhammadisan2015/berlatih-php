<?php
function ubah_huruf($string){
    $result = "";
    for($i = 0; $i < strlen($string); $i++){
        if($string[$i] == 'z'){
            $result .= 'a';
        } else{
            $result .= chr(ord($string[$i])+1);
        }
    }
    return $result;
}
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>